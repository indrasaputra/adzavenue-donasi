package com.donasi.aa.helper;

import com.android.volley.VolleyError;

/**
 * Created by finoindrasaputra on 17/09/18.
 */

public interface VolleyCallback{
    void onSuccess(String response);
    void onError(VolleyError error);
}
