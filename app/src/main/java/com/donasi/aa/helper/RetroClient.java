package com.donasi.aa.helper;

import com.donasi.aa.api.Apiservice;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//////*Koneksi ke Server
public class RetroClient {

    /********
     * URLS
     *******/
    public static final String ROOT_URL = "http://202.43.164.203/";

    /**
     * Get Retrofit Instance
     */
    public static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static Apiservice getApiService() {
        return getRetrofitInstance().create(Apiservice.class);
    }
}