package com.donasi.aa.helper;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by finoindrasaputra on 17/09/18.
 */

public class VolleyHelper {
    private final Context mContext;
    private final RequestQueue mRequestQueue;
    private final String mBaseUrl;

    public VolleyHelper(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(mContext);
        mBaseUrl = "http://202.43.164.203"; //getBaseUrl();
    }

    public void cancelRequest() {
        mRequestQueue.cancelAll("volleyReq");
        System.out.println("volley canceled");
    }

    private String constructUrl(String method) {
        return mBaseUrl + "/" + method;
    }

    public void get(String method, JSONObject jsonRequestParams, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //construct query get
        /*Iterator<String> iter = jsonRequestParams.keys();
        int i = 1;
        while (iter.hasNext()) {
            String key = iter.next();
            Object value = null;
            try {
                value = jsonRequestParams.get(key);
            } catch (JSONException e) {

            }
            if (i == 1) {
                method = method + "?" + key + "=" + value.toString();
            } else {
                method = method + "&" + key + "=" + value.toString();
            }
            iter.remove(); // avoids a ConcurrentModificationException
            i++;
        }*/
        StringRequest objRequest = new StringRequest(Request.Method.GET, constructUrl(method), listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    headers.put("Cookie", "cookies");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return headers;
            }

            @Override
            public Response<String> parseNetworkResponse(NetworkResponse response) {
                System.out.println("server status code " + response.statusCode);
                Map<String, String> responseHeaders = response.headers;
                return super.parseNetworkResponse(response);
            }
        };

        System.out.println("get " + objRequest.getUrl());
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objRequest.setRetryPolicy(policy);
        mRequestQueue.add(objRequest);
    }

    public void put(String method, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.PUT, constructUrl(method), jsonRequest, listener, errorListener);
        mRequestQueue.add(objRequest);
    }

    public void post(String method, JSONObject jsonRequestParams, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //construct params
        //looping jsonRequest, convert to Map
        final Map<String, String> params = new HashMap<String, String>();
        //construct parameter post
        Iterator<String> iter = jsonRequestParams.keys();
        int i = 1;
        while (iter.hasNext()) {
            String key = iter.next();
            Object value = null;
            try {
                value = jsonRequestParams.get(key);
            } catch (JSONException e) {

            }
            params.put(key, value.toString());
            iter.remove(); // avoids a ConcurrentModificationException
            i++;
        }
        StringRequest objRequest = new StringRequest(Request.Method.POST, constructUrl(method), listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    headers.put("Cookie", "cookies");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return headers;
            }

            @Override
            public Response<String> parseNetworkResponse(NetworkResponse response) {
                Map<String, String> responseHeaders = response.headers;
                String rawCookies = responseHeaders.get("Set-Cookie");
                if (rawCookies != null) {
                    //CookieExtractor cookieExtractor = new CookieExtractor(rawCookies);
                    System.out.println("server status code " + response.statusCode);
                    //System.out.println("cookie:" + cookieExtractor.getCookie()
                      //      + "; Expire:" + cookieExtractor.getExpire());
                    //hawkHelper.setString("cookies", cookieExtractor.getCookie());
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        System.out.println("post " + objRequest.getUrl());
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objRequest.setRetryPolicy(policy);
        mRequestQueue.add(objRequest);
    }



}
