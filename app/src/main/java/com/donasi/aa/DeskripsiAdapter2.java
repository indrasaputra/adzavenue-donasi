package com.donasi.aa;



import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.donasi.dd.retrofit2.R;

import java.util.ArrayList;


//Duplikat dari class deskripsiAdapter
public class DeskripsiAdapter2 extends RecyclerView.Adapter<DeskripsiAdapter2.ViewHolder> implements Filterable{


    private ArrayList<Donasi> donasi2;
    private ArrayList<Donasi> mfilteredlist2;

//    private static final int LAYOUT_ONE = 0;
//    private static final int LAYOUT_TWO = 1;




//    @Override
//    public int getItemViewType(int position)
//    {
//        if(position==0)
//            return LAYOUT_ONE;
//
//        else
//            return LAYOUT_TWO;
//
//    }
    public DeskripsiAdapter2(ArrayList<Donasi> donasi2) {
        this.donasi2 = donasi2;
        mfilteredlist2 = donasi2;
    }


    @Override
    public DeskripsiAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pembayaran , parent,false);
        return new DeskripsiAdapter2.ViewHolder(view);


//        View view = null;
//        RecyclerView.ViewHolder viewHolder = null;
//
//        if (viewType == LAYOUT_ONE) {
//            view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.card_donasi, parent, false );
//
//        } else {
//            view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.pembayaran, parent, false );
//
//        }
//        return new DeskripsiAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(DeskripsiAdapter2.ViewHolder holder, int position) {
//        if(holder.getItemViewType()== LAYOUT_ONE) {
        holder.event_target2.setText( donasi2.get( position ).getEvent_target2() );
        holder.event_real2.setText( donasi2.get( position ).getEvent_real2() );
        holder.event_achievment2.setProgress( Integer.parseInt( donasi2.get(position).getEvent_achievment2() ) ) ;
//        }


//        else {


//        }

    }


    @Override
    public int getItemCount() {
        return donasi2.size();
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mfilteredlist2 = donasi2;
                } else {

                    ArrayList<Donasi> filteredList = new ArrayList<>();

                    for (Donasi androidVersion2 : donasi2) {

                        if (androidVersion2.getEvent_real2().toLowerCase().contains(charString) || androidVersion2.getEvent_target2().toLowerCase().contains(charString) || androidVersion2.geteventName().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion2);
                        }
                    }

                    mfilteredlist2 = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist2;
                return filterResults;
            }



            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mfilteredlist2 = (ArrayList<Donasi>) filterResults.values;
                notifyDataSetChanged();


            }
        };


    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView event_target2;
        private TextView event_real2;
        private ProgressBar event_achievment2;


        public ViewHolder(View itemView) {
            super(itemView);

            event_target2 = (TextView) itemView.findViewById(R.id.end_target2);
            event_real2 = (TextView)  itemView.findViewById(R.id.start_target2);

            event_achievment2 = (ProgressBar) itemView.findViewById(R.id.progress_text2);



        }

    }


}
