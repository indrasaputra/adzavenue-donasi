package com.donasi.aa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.donasi.aa.api.Apiservice;
import com.donasi.aa.helper.RetroClient;
import com.donasi.dd.retrofit2.R;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/////*Menampilkan ACTIVIY KE 4

public class Pembayaran extends AppCompatActivity {

    ProgressBar progressBar;
    TextView editnama, editemail, editnomorhp, editjumlah, editcatatan;

    private RecyclerView recyclerView1;

    private ArrayList<donasi> data;
    private deskripsiAdapter adapter;

    ExpandableRelativeLayout expandableLayout;
    TextView txt_expand;
    Button btn_donasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.pembayaran );

        initViews();

        progressBar = (ProgressBar) findViewById( R.id.progress );

        editnama = (EditText) findViewById( R.id.txt_name );
        editemail = (EditText) findViewById( R.id.txt_email );
        editnomorhp = (EditText) findViewById( R.id.txt_hp );
        editjumlah = (EditText) findViewById( R.id.jumlah );
        editcatatan = (EditText) findViewById( R.id.catatan );


        findViewById( R.id.btn_donas ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if user pressed on button register
                //here we will register the user to server

                final String username = editnama.getText().toString().trim();
                final String email = editemail.getText().toString().trim();
                final String nomorhp = editnomorhp.getText().toString().trim();
                final String jumlah = editjumlah.getText().toString().trim();
                final String catatan = editcatatan.getText().toString().trim();



                /////*FUNGSI JIKA FORM BELUM DI ISI TAPI LANGSUNG MENG KLIK BUTTON "PILIH METODE PEMBAYARAN"
                if (TextUtils.isEmpty( username )) {
                    editnama.setError( "Harap masukkan nama" );
                    editnama.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty( email )) {
                    editemail.setError( "Harap masukkan email" );
                    editemail.requestFocus();

                    return;
                }

                if (TextUtils.isEmpty( nomorhp )) {
                    editnomorhp.setError( "Harap masukkan telepon" );
                    editnomorhp.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty( jumlah )) {
                    editjumlah.setError( "Harap masukkan Jumlah" );
                    editjumlah.requestFocus();
                    return;
                }


                registerUser();
            }
        } );
    }

    private void registerUser() {

        Intent intent1 = new Intent( Pembayaran.this, MetodePembayaran.class );
        intent1.putExtra( "username", editnama.getText().toString() );
        intent1.putExtra( "email", editemail.getText().toString() );
        intent1.putExtra( "jumlah", editjumlah.getText().toString() );
        intent1.putExtra( "msisdn", editnomorhp.getText().toString() );


        //startActivity(intent1);

        startActivityForResult( intent1, 1 );
    }


    private void initViews() {

        btn_donasi = (Button) findViewById( R.id.btn_donas );

        recyclerView1 = (RecyclerView) findViewById( R.id.info_donasie );

        recyclerView1.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( this, 1 );
        //       recyclerView.addItemDecoration(new MainActivity.GridSpacingItemDecoration(2, dpToPx(10), true));
//       RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView1.setLayoutManager( layoutManager );

        loadJSON();
    }


    private void loadJSON() {
        Apiservice api = RetroClient.getApiService();
        Call<JSONResponse> call = api.getdonasi();
        call.enqueue( new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>( Arrays.asList( jsonResponse.getdonasi() ) );
                adapter = new deskripsiAdapter( data );
                recyclerView1.setAdapter( adapter );
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d( "Error", t.getMessage() );
            }


        } );

    }
}


