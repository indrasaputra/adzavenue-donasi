package com.donasi.aa;

public class AppVar {

    //////*Untuk Request URL

    public static final String URL_CC = "http://202.43.164.203/payment/finpay/paycc.php";

    public static final String LOGIN_URL = "http://202.43.164.203/payment/finpay/testing.php";

    public static final String KEY_EMAIL = "email";

    public static final String KEY_JUMLAH = "jumlah";

    public static final String KEY_MSISDN = "msisdn";

    public static final String KEY_USER = "nama";

    public static final String KEY_SOFTID = "sofid";

    public static final String LOGIN_SUCCESS = "success";

}
