package com.donasi.aa;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.donasi.aa.controller.ControllerDonasi;
import com.donasi.aa.helper.VolleyCallback;
import com.donasi.dd.retrofit2.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<CategoryDatasource> dataSourceList;
    private CategoryAdapter adapter;
    private ProgressDialog progressDialog;
    private ControllerDonasi controllerDonasi;
    private String[] id;
    private String[] category;
    private String[] sub_category;
    CategoryActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        myApp = this;

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        dataSourceList = new ArrayList<CategoryDatasource>();
        adapter = new CategoryAdapter(dataSourceList);
        recyclerView.setAdapter(adapter);

        loadCategory();
    }

    public void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        dataSourceList = new ArrayList<CategoryDatasource>();
        adapter = new CategoryAdapter(dataSourceList);
        recyclerView.setAdapter(adapter);

        for (int i=0; i<id.length; i++) {
            CategoryDatasource a = new CategoryDatasource(id[i], category[i], sub_category[i], ColorUtil.getRandomColor());
            dataSourceList.add(a);
        }
        adapter.notifyDataSetChanged();
    }

    public void loadCategory() {
        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);

        myApp.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
            }
        });

        controllerDonasi = new ControllerDonasi(this);
        controllerDonasi.getCategory(new VolleyCallback() {
            @Override
            public void onSuccess(String response) {
                System.out.println("Response ===== " + response);
                if (response!=null) {
                    try {
                        JSONObject jobjResp = new JSONObject(response);
                        JSONArray jsonArrayAndroid = jobjResp.getJSONArray("android");
                        id = new String[jsonArrayAndroid.length()];
                        category = new String[jsonArrayAndroid.length()];
                        sub_category = new String[jsonArrayAndroid.length()];
                        for (int i=0; i<jsonArrayAndroid.length(); i++) {
                            JSONObject android = jsonArrayAndroid.getJSONObject(i);
                            id[i] = android.getString("id");
                            category[i] = android.getString("category");
                            sub_category[i] = android.getString("sub_category");
                        }
                        initViews();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    Toast.makeText(myApp, "Server error", Toast.LENGTH_LONG).show();
                }
                myApp.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(myApp, "Error koneksi", Toast.LENGTH_LONG).show();
                myApp.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });

            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;

        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
