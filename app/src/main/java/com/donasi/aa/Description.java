package com.donasi.aa;

import android.support.v4.view.MenuItemCompat;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.donasi.aa.api.Apiservice;
import com.donasi.aa.helper.RetroClient;
import com.donasi.dd.retrofit2.R;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/////*Menampilkan activity ke 3

public class Description extends AppCompatActivity {
    public static final String ROOT_URL = "http://202.43.164.203";

    private RecyclerView recyclerView;
    private ArrayList<Donasi> data;
    private DeskripsiAdapter adapter;

    ExpandableRelativeLayout expandableLayout;
    TextView txt_expand;
    Button btn_donasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.content);

        initViews();
    }

    public void btn_donasi(View view){
        btn_donasi = (findViewById(R.id.btn_donasi));

        Intent intent = new Intent(Description.this, Pembayaran.class);
        Description.this.startActivity(intent);
        Description.this.finish();


    }
    private void initViews() {

        //menampilkan data2 ke recycleview (Realisasi,target,progresbar,event name, & deskripsi event)

        btn_donasi = (Button) findViewById(R.id.btn_donasi);
        recyclerView = (RecyclerView) findViewById(R.id.info_donasi);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        //       recyclerView.addItemDecoration(new MainActivity.GridSpacingItemDecoration(2, dpToPx(10), true));
//       RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        loadJSON();
    }

    private void loadJSON() {

        Apiservice api = RetroClient.getApiService();
        Call<JSONResponse> call = api.getdonasi();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getdonasi()));
                adapter = new DeskripsiAdapter(data);
                recyclerView.setAdapter(adapter);


            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //Menampilkan fungsi searchview di activity ke 3

        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);




        search(searchView);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }



            @Override
            public boolean onQueryTextChange(String newText) {

                if (adapter != null) adapter.getFilter().filter(newText);
                return true;
            }
        });
    }
}
