package com.donasi.aa;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.donasi.dd.retrofit2.R;

import java.util.ArrayList;

public class subcatAdapter extends RecyclerView.Adapter<subcatAdapter.ViewHolder>{


    private ArrayList<category> android;

    public subcatAdapter(ArrayList<category> android) {
        this.android = android;
    }

    @Override
    public subcatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_row,parent,false);
        return new subcatAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(subcatAdapter.ViewHolder holder, int position) {

        category category = android.get( position );
        holder.itemView.setBackgroundColor( category.color );

        holder.sub_category.setText(android.get(position).getSub_category());


    }


    @Override
    public int getItemCount() {
        return android.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView sub_category;


        public ViewHolder(View itemView) {
            super(itemView);


            sub_category = (TextView)itemView.findViewById(R.id.sub_kat);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), Description.class);
                    v.getContext().startActivity(intent);


                }
            });
        }
    }
}
