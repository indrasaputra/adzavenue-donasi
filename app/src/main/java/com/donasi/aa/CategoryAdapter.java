package com.donasi.aa;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.donasi.dd.retrofit2.R;

import java.util.ArrayList;

/**
 * Created by Roby on 09.05.2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


    private ArrayList<category> android;
    private ArrayList<CategoryDatasource> datasourceArrayList;

    /*public CategoryAdapter(ArrayList<category> android) {
        this.android = android;
    }*/

    public CategoryAdapter(ArrayList<CategoryDatasource> datasourceArrayList) {
        this.datasourceArrayList = datasourceArrayList;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.ViewHolder holder, int position) {
        CategoryDatasource category = datasourceArrayList.get( position );
        holder.itemView.setBackgroundColor( category.getColor() );
        holder.tv_version.setText(category.getCategory());
    }

    @Override
    public int getItemCount() {
        return datasourceArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_version;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_version = (TextView)itemView.findViewById(R.id.tv_version);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(v.getContext(), Activity_subcategory.class);
                    v.getContext().startActivity(i);

                }
            });

        }
    }
}
