package com.donasi.aa;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.donasi.dd.retrofit2.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class deskripsiAdapter extends RecyclerView.Adapter<deskripsiAdapter.ViewHolder> implements Filterable{


    private ArrayList<donasi> donasi;
    private ArrayList<donasi> mfilteredlist;

    private static final int LAYOUT_ONE= 0;
    private static final int LAYOUT_TWO= 1;


    @Override
    public int getItemViewType(int position)
    {
        if(position==0)
            return LAYOUT_ONE;
        else
            return LAYOUT_TWO;

    }
    public deskripsiAdapter(ArrayList<donasi> donasi) {
        this.donasi = donasi;
        mfilteredlist = donasi;
    }


    @Override
    public deskripsiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_donasi , parent,false);
//        return new deskripsiAdapter.ViewHolder(view);


        View view = null;
        RecyclerView.ViewHolder viewHolder = null;

        if (viewType == LAYOUT_ONE) {
            view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.card_donasi, parent, false );

        } else {
            view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.pembayaran, parent, false );

        }
     return new deskripsiAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(deskripsiAdapter.ViewHolder holder, int position) {
        if(holder.getItemViewType()== LAYOUT_ONE) {
            holder.event_name.setText( donasi.get( position ).geteventName() );
            holder.event_desc.setText( donasi.get( position ).getEventDesc() );
            holder.event_target.setText( donasi.get( position ).getEvent_target() );
            holder.event_real.setText( donasi.get( position ).getEvent_real() );
            holder.event_achievment.setText( donasi.get( position ).getevent_achievment() );
        }


        else {

            holder.event_target.setText( donasi.get( position ).getEvent_target() );
            holder.event_real.setText( donasi.get( position ).getEvent_real() );
            holder.event_achievment.setText( donasi.get( position ).getevent_achievment() );

        }
    }




    @Override
    public int getItemCount() {
        return donasi.size();
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mfilteredlist = donasi;
                } else {

                    ArrayList<donasi> filteredList = new ArrayList<>();

                    for (donasi androidVersion : donasi) {

                        if (androidVersion.geteventName().toLowerCase().contains(charString) || androidVersion.getEventDesc().toLowerCase().contains(charString) || androidVersion.getevent_achievment().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mfilteredlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist;
                return filterResults;
            }



            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mfilteredlist = (ArrayList<donasi>) filterResults.values;
                notifyDataSetChanged();


            }
        };


    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView event_name;
        private TextView event_desc;
        private TextView event_target;
        private TextView event_real;
        private TextView event_achievment;



        public ViewHolder(View itemView) {
            super(itemView);

            event_name = (TextView)itemView.findViewById(R.id.event_name);
            event_desc = (TextView)itemView.findViewById(R.id.detail);
            event_target = (TextView) itemView.findViewById(R.id.end_target);
            event_real = (TextView)  itemView.findViewById(R.id.start_target);
            event_achievment = (TextView) itemView.findViewById(R.id.progress_text);


        }
    }

}