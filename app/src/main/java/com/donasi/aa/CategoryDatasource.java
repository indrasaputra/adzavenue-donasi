package com.donasi.aa;

/**
 * Created by Roby on 08.05.2018.
 */

public class CategoryDatasource {
    public int color;
    public String id;
    public String category;
    public String sub_category;

    public CategoryDatasource(String id, String category, String sub_category, int randomcolor) {
        this.id = id;
        this.category = category;
        this.sub_category = sub_category;
        this.color = randomcolor;
    }

    public CategoryDatasource(String category, int randomcolor) {
        this.category = category;
        this.sub_category = sub_category;
        this.color = randomcolor;
    }

    public String getCategory() {
        return category;
    }

    public String getId() {
        return id;
    }

    public int getColor() {
        return color;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setApi(String category) {
        this.sub_category = category;
    }
}
