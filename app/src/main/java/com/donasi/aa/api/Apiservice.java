package com.donasi.aa.api;

import com.donasi.aa.JSONResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/////*Untuk Mengambil data dari file php

public interface Apiservice {

    @GET("payment/cobain.php")
    Call<JSONResponse> getMyJSON();

    @GET("payment/subcategory.php")
    Call<JSONResponse> getdonasi();

    @GET("payment/subcategory.php")
    Call<JSONResponse> getdonasi2();
}
