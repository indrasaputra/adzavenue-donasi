package com.donasi.aa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.donasi.aa.api.Apiservice;
import com.donasi.aa.helper.RetroClient;
import com.donasi.dd.retrofit2.R;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//////*Menampilakan Activity ke 2

public class Activity_subcategory extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<category> data;
    private SubcatAdapter subadapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        getSupportActionBar().hide();

        initViews();
    }


    private void initViews() {

        //menampilkan data di recycleview
        recyclerView = (RecyclerView) findViewById(R.id.submenu_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }


    private void loadJSON() {

        //meload data-data JSON
        Apiservice api = RetroClient.getApiService();

        Call<JSONResponse> call = api.getMyJSON();


        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
                subadapter = new SubcatAdapter(data);
                recyclerView.setAdapter(subadapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
}