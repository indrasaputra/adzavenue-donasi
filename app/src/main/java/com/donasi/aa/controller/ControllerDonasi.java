package com.donasi.aa.controller;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.donasi.aa.helper.VolleyCallback;
import com.donasi.aa.helper.VolleyHelper;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by finoindrasaputra on 14/09/18.
 */

public class ControllerDonasi {
    Context context;
    VolleyHelper volley;

    public ControllerDonasi(Context context) { this.context = context; }

    public void cancelRequest(){
        if(volley != null){
            volley.cancelRequest();

        }
    }

    public void getCategory(final VolleyCallback volleyCallback) {
        volley = new VolleyHelper(context);
        volley.get("/payment/cobain.php", new JSONObject(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallback.onError(error);
                    }
                });
    }
}
