package com.donasi.aa;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.donasi.dd.retrofit2.R;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


///*Menampilakan Activity ke 5
public class MetodePembayaran extends Activity {

    TextView TextViewJumlah;
    TextView total;

    ExpandableRelativeLayout expandableLayout1, expandableLayout2, expandableLayout3;
    private Context context;
    private Button Payfincode;
    private Button PayvirtualAccount;
    private Button creditcardbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_metode);

    }


    //membuat fungsi tombol finpaycode ,serta menampilkan isi di dalam tombol tersebut

    public void expandableButton1(View view) {

        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);

        expandableLayout1.toggle(); // toggle expand and collapse

        TextViewJumlah  =(TextView) findViewById(R.id.txt_amount);

        total  =(TextView) findViewById(R.id.txt_jumlah);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        final String jumlah = bundle.getString("jumlah");
        String Subtotal  = bundle.getString("jumlah");
        final String nama =bundle.getString("username");
        final String email =bundle.getString("email");
        final String msisdn = bundle.getString("msisdn");
        final String softid ="finpay021";

        TextViewJumlah.setText(jumlah.toString());
        total.setText(Subtotal.toString());

        Payfincode = (Button) findViewById(R.id.btn_finpaycode);

        Payfincode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finpay();
            }



            private void finpay() {


                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppVar.LOGIN_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                Log.i("hasil", response);


                                try {
                                    //Do it with this it will work
                                    JSONObject person = new JSONObject(response);
                                    String kode = person.getString("payment_code");

                                    //         Toast.makeText(MetodePembayaran.this, kode, Toast.LENGTH_SHORT).show();

                                    Intent i = new Intent (MetodePembayaran.this, PaymentCode.class);
                                    i.putExtra("kodefinpay", kode);
                                    startActivity(i);



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //You can handle error here if you want

                                Toast.makeText(MetodePembayaran.this, "gagal terhubung", Toast.LENGTH_SHORT).show();

                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        //Adding parameters to request
                        params.put(AppVar.KEY_EMAIL, email);
                        params.put(AppVar.KEY_JUMLAH, jumlah);
                        params.put(AppVar.KEY_USER, nama);
                        params.put(AppVar.KEY_MSISDN, msisdn);
                        params.put(AppVar.KEY_SOFTID, softid);

                        //returning parameter
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(
                        new DefaultRetryPolicy(
                                500000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        )
                );


                //Adding the string request to the queue
                Volley.newRequestQueue(MetodePembayaran.this).add(stringRequest);
            }
        });
    }


    //*MEMBUAT fungsi tombol CREDIT CARD ,serta menampilkan isi di dalam tombol tersebut

    public void expandableButton2(View view) {
        expandableLayout2 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout2);
        expandableLayout2.toggle(); // toggle expand and collapse

        TextViewJumlah  =(TextView) findViewById(R.id.txt_amount2);

        total  =(TextView) findViewById(R.id.txt_jumlah2);

        Intent intent = getIntent();


        Bundle bundle = intent.getExtras();
        final String jumlah = bundle.getString("jumlah");
        String Subtotal  = bundle.getString("jumlah");
        final String nama =bundle.getString("username");
        final String email =bundle.getString("email");
        final String msisdn = bundle.getString("msisdn");
        final String softid ="cc";


        TextViewJumlah.setText(jumlah.toString());
        total.setText(Subtotal.toString());

        creditcardbtn = (Button) findViewById(R.id.creditcard);

        creditcardbtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                paymentcc();

            }

            private void paymentcc() {


                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppVar.URL_CC,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                Log.i("hasil", response);


                                try {
                                    //Do it with this it will work
                                    JSONObject person = new JSONObject(response);
                                    String redirect = person.getString("redirect_url");

                                    //         Toast.makeText(MetodePembayaran.this, kode, Toast.LENGTH_SHORT).show();
//
                                    //                                   Intent i = new Intent(Intent.ACTION_VIEW,
                                    //                                           Uri.parse(redirect));
                                    //                                   startActivity(i);

                                    Intent i = new Intent (MetodePembayaran.this, PaymentCc.class);
                                    i.putExtra("url", redirect);
                                    startActivity(i);





                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        },


                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //You can handle error here if you want

                                Toast.makeText(MetodePembayaran.this, "gagal terhubung", Toast.LENGTH_SHORT).show();

                            }


                        })

                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        //Adding parameters to request
                        params.put(AppVar.KEY_EMAIL, email);
                        params.put(AppVar.KEY_JUMLAH, jumlah);
                        params.put(AppVar.KEY_USER, nama);
                        params.put(AppVar.KEY_MSISDN, msisdn);
                        params.put(AppVar.KEY_SOFTID, softid);

                        //returning parameter
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(
                        new DefaultRetryPolicy(
                                500000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        )
                );

                //Adding the string request to the queue
                Volley.newRequestQueue(MetodePembayaran.this).add(stringRequest);


            }
        });

    }


    //*MEMBUAT fungsi tombol VIRTUAL ACCOUNT BNI ,serta menampilkan isi di dalam tombol tersebut


    public void expandableButton3(View view) {
        expandableLayout3 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout3);
        expandableLayout3.toggle(); // toggle expand and collapse


        TextViewJumlah  =(TextView) findViewById(R.id.txt_amount3);

        total  =(TextView) findViewById(R.id.txt_jumlah3);

        Intent intent = getIntent();


        Bundle bundle = intent.getExtras();
        final String jumlah = bundle.getString("jumlah");
        String Subtotal  = bundle.getString("jumlah");
        final String nama =bundle.getString("username");
        final String email =bundle.getString("email");
        final String msisdn = bundle.getString("msisdn");
        final String softid ="vastbni";


        TextViewJumlah.setText(jumlah.toString());
        total.setText(Subtotal.toString());

        PayvirtualAccount = (Button) findViewById(R.id.btn_virtual);



        PayvirtualAccount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                virtualbni();
            }

            private void virtualbni() {


                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppVar.LOGIN_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                Log.i("hasil", response);


                                try {
                                    //Do it with this it will work
                                    JSONObject person = new JSONObject(response);
                                    String kode = person.getString("payment_code");

                                    //         Toast.makeText(MetodePembayaran.this, kode, Toast.LENGTH_SHORT).show();

                                    Intent i = new Intent (MetodePembayaran.this, PaymentCode.class);
                                    i.putExtra("kodefinpay", kode);
                                    startActivity(i);





                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        },


                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //You can handle error here if you want

                                Toast.makeText(MetodePembayaran.this, "gagal terhubung", Toast.LENGTH_SHORT).show();

                            }

                        })


                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        //Adding parameters to request
                        params.put(AppVar.KEY_EMAIL, email);
                        params.put(AppVar.KEY_JUMLAH, jumlah);
                        params.put(AppVar.KEY_USER, nama);
                        params.put(AppVar.KEY_MSISDN, msisdn);
                        params.put(AppVar.KEY_SOFTID, softid);

                        //returning parameter
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(
                        new DefaultRetryPolicy(
                                500000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        )
                );

                //Adding the string request to the queue
                Volley.newRequestQueue(MetodePembayaran.this).add(stringRequest);


            }
        });
    }

}