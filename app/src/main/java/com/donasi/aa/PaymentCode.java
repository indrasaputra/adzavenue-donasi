package com.donasi.aa;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.donasi.dd.retrofit2.R;

//*
////BERHUBUNGAN DENGAN BUTTON Finpay Code Di MetodePembayaran (Payment gateway finpay021)

public class PaymentCode extends AppCompatActivity {

    Button showpayment;
    TextView textkodefinpay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_code);

        textkodefinpay = (TextView) findViewById(R.id.kde_pay);

        Intent i = getIntent();
        String kodePay = i.getStringExtra("kodefinpay");

        textkodefinpay.setText(kodePay.toString());

        }


    public void clip(View view) {
        // Code to Copy the content of Text View to the Clip board.

        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("simple text", textkodefinpay.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getApplicationContext(), "Teks berhasil di salin",
                Toast.LENGTH_LONG).show();
    }




    public void Pay(View view) {
        showpayment = (findViewById(R.id.btn_kembaliberanda));

        Intent intent = new Intent(PaymentCode.this, MainActivity.class);

        PaymentCode.this.startActivity(intent);
        PaymentCode.this.finish();


    }
}
