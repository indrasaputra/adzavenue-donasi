package com.donasi.aa;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;

import com.donasi.aa.api.Apiservice;
import com.donasi.aa.helper.RetroClient;
import com.donasi.dd.retrofit2.R;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//*Menampilkan Activity 1
public class MainActivity extends AppCompatActivity{

    private RecyclerView recyclerView;
    private ArrayList<category> data;
    private CategoryAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        initViews();
    }

    private void initViews() {
        //menampilakan data-data kategori

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }


    private void loadJSON() {

        Apiservice api = RetroClient.getApiService();
        Call<JSONResponse> call = api.getMyJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {


                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));

                //14-09-2018
                //adapter = new CategoryAdapter(getData());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    public ArrayList<category> getData() {

        ////*Menampilkan 4 kategori
        ArrayList<category> android = new ArrayList<>();
        category data = new category("ZakaTEL", ColorUtil.getRandomColor());
        android.add(data);


        data = new category("Bedah Rumah", ColorUtil.getRandomColor());
        android.add(data);

        data = new category("Pensiun Berdaya", ColorUtil.getRandomColor());
        android.add(data);

        data = new category("Galang Dana", ColorUtil.getRandomColor());
        android.add(data);


        return android;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
