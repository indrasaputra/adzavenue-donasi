-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_transaction`;
CREATE TABLE `tbl_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` varchar(128) NOT NULL,
  `cust_name` varchar(128) NOT NULL,
  `cust_email` varchar(128) NOT NULL,
  `msisdn` varchar(128) NOT NULL,
  `ammount` varchar(128) NOT NULL,
  `sofid` varchar(128) NOT NULL,
  `invoice` varchar(128) NOT NULL,
  `add_info` varchar(128) NOT NULL,
  `trans_date` datetime NOT NULL,
  `status_desc` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_transaction` (`id`, `cust_id`, `cust_name`, `cust_email`, `msisdn`, `ammount`, `sofid`, `invoice`, `add_info`, `trans_date`, `status_desc`, `payment_code`) VALUES
(206,	'efb0506fd',	'rudi',	'andrisejati26@gmail.com',	'083811108839',	'250000',	'cc',	'239739169',	'',	'2018-08-23 14:36:04',	'',	''),
(207,	'86e90dd96',	'budi',	'budi@gmail.com',	'083811108838',	'50000',	'cc',	'4dfa8b3aa',	'',	'2018-08-23 14:40:27',	'',	''),
(208,	'eaf7bb3fb',	'budi',	'budi@gmail.com',	'083811108838',	'50000',	'cc',	'6a57c1d60',	'',	'2018-08-23 14:41:59',	'',	''),
(209,	'0fbebabf5',	'andri',	'andrisejati26@gmail.com',	'083811108839',	'50000',	'cc',	'6d6dad26e',	'',	'2018-08-23 14:51:14',	'',	''),
(210,	'840c28eb5',	'dony',	'dony@gmail.com',	'083811108839',	'50000',	'cc',	'42abcb832',	'',	'2018-08-23 15:03:19',	'',	''),
(211,	'34dac678c',	'andri',	'andrisejati26@gmail.com',	'083811108839',	'50000',	'cc',	'7635803fc',	'',	'2018-08-23 15:19:43',	'',	'');

-- 2018-08-24 06:38:50
